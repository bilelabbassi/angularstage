import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  employeename: string ="";
  email: string ="";
  societe: string="";
  password: string ="";


  constructor(private http: HttpClient )
  {

  }
  save()
  {
  
    let bodyData = {
      "username" : this.employeename,
      "email" : this.email,
      "societe" : this.societe,
      "password" : this.password
    };
    this.http.post("http://localhost:8000/auth/register",bodyData,{responseType: 'text'}).subscribe((resultData: any)=>
    {
        console.log(resultData);
        alert("Employee Registered Successfully");

    });
  }

}