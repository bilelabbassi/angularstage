import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
 

  employeename: string ="";
  password: string ="";



  constructor(private router: Router,private http: HttpClient) {}
 


  Login() {
    console.log(this.employeename);
    console.log(this.password);
 
    let bodyData = {
      username: this.employeename,
      password: this.password,
    };
 
        this.http.post("http://localhost:8000/auth/login", bodyData).subscribe(  (resultData: any) => {
        console.log("**"+resultData?.user?.authorities[0]?.authority);
 
        if (resultData?.user?.authorities[0]?.authority == "USER")
        {
      
          this.router.navigateByUrl('/userhome');
    
 
        }
        else if(resultData?.user?.authorities[0]?.authority == "ADMIN")
    
         {
          this.router.navigateByUrl('/home');
        }
        else
        {
          alert("Incorrect username and Password not match");
        }

      });
    }

}